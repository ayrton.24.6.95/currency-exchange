package com.bcp.amount.dto;

import java.math.BigDecimal;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Table("ExchangeRate")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class CurrencyExchange {

	@Id
	private Integer id;
	@Column(value = "coin_type")
	private BigDecimal coinType;
	private String origin;
	private String destination;

}
