package com.bcp.amount.request;

import java.math.BigDecimal;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CurrencyConversionRq {

	@Size(min = 0)
	private BigDecimal amount;

	@NotNull(message = "El campo originCurrency no puede ser nulo")
	@NotBlank(message = "El campo originCurrency no se puede enviar el monto en blanco")
	private String originCurrency;

	@NotNull(message = "El campo destinationCurrency no puede ser nulo")
	@NotBlank(message = "El campo destinationCurrency no se puede enviar el monto en blanco")
	private String destinationCurrency;
}
