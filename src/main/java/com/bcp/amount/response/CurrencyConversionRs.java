package com.bcp.amount.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class CurrencyConversionRs {

	private String amount;
	private String amountExchangeRate;
	private String originCurrency;
	private String destinationCurrency;
	private String exchangeRate;
}
