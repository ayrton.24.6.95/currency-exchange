package com.bcp.amount.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.bcp.amount.dto.CurrencyExchange;
import com.bcp.amount.repository.CurrencyExchangeRepository;
import com.bcp.amount.request.CurrencyConversionRq;
import com.bcp.amount.response.CurrencyConversionRs;
import com.bcp.amount.service.AmountService;

import lombok.AllArgsConstructor;
import reactor.core.publisher.Mono;

@Service
@AllArgsConstructor
public class AmountServiceImpl implements AmountService {

	private CurrencyExchangeRepository repository;

	@Override
	public Mono<CurrencyConversionRs> conversionCurrency(CurrencyConversionRq request) {

		return repository.findByOriginAndDestination(request.getOriginCurrency(), request.getDestinationCurrency())
				.map(currencyExchange -> {

					return CurrencyConversionRs.builder().amount(request.getAmount().toString())
							.amountExchangeRate((request.getAmount().multiply(currencyExchange.getCoinType()))
									.setScale(2).toString())
							.originCurrency(request.getOriginCurrency())
							.destinationCurrency(request.getDestinationCurrency())
							.exchangeRate(currencyExchange.getCoinType().toString()).build();

				});

	}


}
