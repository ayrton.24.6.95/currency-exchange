package com.bcp.amount.service;

import com.bcp.amount.request.CurrencyConversionRq;
import com.bcp.amount.response.CurrencyConversionRs;

import reactor.core.publisher.Mono;

public interface AmountService {

	Mono<CurrencyConversionRs> conversionCurrency(CurrencyConversionRq request);

}
