package com.bcp.amount.repository;

import org.springframework.data.r2dbc.repository.R2dbcRepository;

import com.bcp.amount.dto.CurrencyExchange;

import reactor.core.publisher.Mono;

public interface CurrencyExchangeRepository extends R2dbcRepository<CurrencyExchange, Integer> {

	Mono<CurrencyExchange> findByOriginAndDestination(String origin,String destination);
}
