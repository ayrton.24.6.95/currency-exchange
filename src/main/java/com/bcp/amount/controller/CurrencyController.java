package com.bcp.amount.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bcp.amount.request.CurrencyConversionRq;
import com.bcp.amount.response.CurrencyConversionRs;
import com.bcp.amount.service.AmountService;

import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "/conversion")
public class CurrencyController {

	@Autowired
	private AmountService amountService;

	@CrossOrigin(value = "*")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public Mono<CurrencyConversionRs> conversionCurrency(@RequestBody @Valid CurrencyConversionRq request) {
		return amountService.conversionCurrency(request);
	}

}
