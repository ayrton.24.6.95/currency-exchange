package com.bcp.amount.controller;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bcp.amount.dto.CurrencyExchange;
import com.bcp.amount.repository.CurrencyExchangeRepository;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "/amount")
@AllArgsConstructor
@Slf4j
public class ExchangeRateAmountController {

	private CurrencyExchangeRepository repository;

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public Flux<CurrencyExchange> getAllBooks() {
		return repository.findAll();
	}

	@GetMapping(value = "/{id}")
	public Mono<CurrencyExchange> findById(@PathVariable Integer id) {
		return repository.findById(id);
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public Mono<CurrencyExchange> save(@RequestBody CurrencyExchange request) {
		return repository.save(request);
	}

	@DeleteMapping(value = "/{id}")
	public Mono<Void> delete(@PathVariable Integer id) {
		return repository.deleteById(id);
	}

	@PostMapping(value = "/cambio")
	public Mono<Void> crearTipoCambioApi(@RequestBody List<CurrencyExchange> request) {
		return crearTipoCambio(request);
	}

	private Mono<Void> crearTipoCambio(List<CurrencyExchange> request) {

		request.forEach(data -> {

			repository.findAll().map(dato -> {
		
				if (dato.getOrigin().equalsIgnoreCase(data.getOrigin())
						&& dato.getDestination().equalsIgnoreCase(data.getDestination())) {
					log.info("Se actualiza el tipo de cambio: {} {}", dato.getOrigin(),dato.getDestination());
					repository.save(CurrencyExchange.builder()
							.id(dato.getId())
							.coinType(data.getCoinType())
							.origin(data.getOrigin())
							.destination(data.getDestination()).build()).subscribe();
					return Mono.empty();

				} else {
					log.info("Se crea el tipo de cambio: {} {}",dato.getOrigin(),dato.getDestination());
					repository.save(CurrencyExchange.builder().coinType(data.getCoinType()).origin(data.getOrigin())
							.destination(data.getDestination()).build()).subscribe();
					return Mono.empty();
				}

			}).subscribe();

		});

		return Mono.empty();
	}
}
