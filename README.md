### Manejo de Versiones

Todo el versioanmiento de ols repositorios se realiza bajo el es flujo de trabajo de GITFLOW

| Ramas | Descripción |
| ------ | ------ |
| master | Rama principal la cual solo debe de encontrarse los cambios que se encuentran en funcionamiento |
| develop | Rama de la cual se tienen que crear las demas ramas |
| release | Rama de nuevos lanzamientos y la unica que interactua con la **"master"** |
| feature | Rama de nuevas funcionalidades |
| hotfix | Se   utilizan para reparar rápidamente las publicaciones de producción |


Para mayor información, o indagar más sobre el flujo de trabajo **GITFLOW** revisar la documentación de [Atlassian](https://www.atlassian.com/es/git/tutorials/comparing-workflows/gitflow-workflow). 

------------------------------------------------------------------------
# **Docker**

Para la creación de la imagen docker a traves del Dockerfile se utilizara el siguiente comando.
Con el siguiente comando:
```sh
docker build -t currency-exchange .
```
Para ejecutar la imagen creada
```sh
docker run -d --name currency-exchange -p 8080:8080 --rm -it demo:latest
```